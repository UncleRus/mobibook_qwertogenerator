package com.qwertovsky.mobibook.generator;

import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

public class FontChooser
{
	private List<String> fontFamilies;
	private Scene scene;
	private Stage stage;
	private ListView<String> fontFamiliesLV;
	private ListView<String> fontNamesLV;
	private ListView<Integer> fontSizesLV;
	
	Font font;
	
	public FontChooser()
	{
		fontFamilies = Font.getFamilies();
		
		// create scene
		BorderPane root = new BorderPane();
		GridPane gridPane = new GridPane();
		fontFamiliesLV = new ListView<String>();
		fontNamesLV = new ListView<String>();
		fontSizesLV = new ListView<Integer>();
		fontFamiliesLV.setMinSize(70, 50);
		fontNamesLV.setMinSize(70, 50);
		fontSizesLV.setMinSize(30, 50);
		gridPane.getChildren().add(fontFamiliesLV);
		gridPane.getChildren().add(fontNamesLV);
		gridPane.getChildren().add(fontSizesLV);
		GridPane.setConstraints(fontFamiliesLV, 0, 0);
		GridPane.setConstraints(fontNamesLV, 1, 0);
		GridPane.setConstraints(fontSizesLV, 2, 0);
		gridPane.setHgap(5);
		
		HBox buttonsBox = new HBox();
		Button useB = new Button("Use");
		Button cancelB = new Button("Cancel");
		buttonsBox.getChildren().addAll(useB, cancelB);
		buttonsBox.setAlignment(Pos.CENTER_RIGHT);
		buttonsBox.setSpacing(5);
		
		root.setCenter(gridPane);
		BorderPane.setMargin(gridPane, new Insets(10,10,10,10));
		root.setBottom(buttonsBox);
		BorderPane.setMargin(buttonsBox, new Insets(0,10,10,10));
		
		fontFamiliesLV.getItems().addAll(fontFamilies);
		fontFamiliesLV.getSelectionModel().selectedItemProperty()
			.addListener(new ChangeListener<String>()
		{

			@Override
			public void changed(
					ObservableValue<? extends String> paramObservableValue,
					String oldFamily, String newFamily)
			{
				List<String> fontNames = Font.getFontNames(newFamily);
				fontNamesLV.getItems().setAll(fontNames);
			}
		});
		fontSizesLV.getItems().addAll(5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19);
		
		useB.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent paramT)
			{
				// get font
				try
				{
					String fontName;
					int fontSize;
					fontName = fontNamesLV.getSelectionModel().getSelectedItem();
					fontSize = fontSizesLV.getSelectionModel().getSelectedItem();
					if(fontName == null || fontSize == 0)
						throw new NullPointerException();
					font = new Font(fontName, fontSize);
					stage.close();
				} catch(NullPointerException npe)
				{
					new MessageBox(stage, "Ошибка", "Укажите шрифт и его размер");
					return;
				}
			}
		});
		
		cancelB.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent paramT)
			{
				stage.close();
			}
		});
		
		scene = new Scene(root, 500, 300);
	}

	//--------------------------------------------
	public Font showChooseDialog(Window owner)
	{
		// create new stage
		stage = new Stage();
		stage.initOwner(owner);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UTILITY);
		stage.setTitle("Шрифт");
		stage.setScene(scene);
		stage.setOnShown(new EventHandler<WindowEvent>()
		{

			@Override
			public void handle(WindowEvent event)
			{
				fontFamiliesLV.scrollTo(fontFamiliesLV.getSelectionModel().getSelectedIndex());
				fontSizesLV.scrollTo(fontSizesLV.getSelectionModel().getSelectedIndex());
			}
			
		});
		stage.setX(owner.getX()+(owner.getWidth()-scene.getWidth()) / 2);
		stage.setY(owner.getY() + (owner.getHeight()-scene.getHeight()) / 2);
		stage.showAndWait();
		return font;
	}
	
	//--------------------------------------------
	public void setInitFont(Font font)
	{
		this.font = font;
		if(font == null)
			return;
		fontFamiliesLV.getSelectionModel().select(font.getFamily());
		
		fontNamesLV.getSelectionModel().select(font.getName());
		
		Integer size = (int)font.getSize();
		fontSizesLV.getSelectionModel().select(size);
		
	}
	
	
}
