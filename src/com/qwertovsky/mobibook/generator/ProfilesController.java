package com.qwertovsky.mobibook.generator;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Profile.Section;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Window;

public class ProfilesController implements Initializable
{
	@FXML
	private TableView<Profile> profileTV;

	@FXML
	private TextField nameTF;
	@FXML
	private TextField widthTF;
	@FXML
	private TextField heightTF;

	@FXML
	private TextField lSoftTF;
	@FXML
	private TextField rSoftTF;
	@FXML
	private TextField menuTF;

	@FXML
	private ChoiceBox<Navigation> onUpCB;
	@FXML
	private ChoiceBox<Navigation> onDownCB;
	@FXML
	private ChoiceBox<Navigation> onLeftCB;
	@FXML
	private ChoiceBox<Navigation> onRightCB;

	private ObservableList<Profile> profilesList;
	private ObservableList<Navigation> navigationActionsList;
	private Ini profilesIni;
	private File profilesFile;
	Window window;
	private ResourceBundle profilesBundle;

	public ProfilesController()
	{
		profilesList = FXCollections.observableArrayList();
		profilesFile = new File("profiles.ini");
	}
	
	//--------------------------------------------
	protected ObservableList<Profile> getProfilesList()
	{
		return profilesList;
	}

	// --------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void saveProfile(ActionEvent event)
	{
		// get selected profile
		int selected = profileTV.getSelectionModel().getSelectedIndex();
		Profile newProfile = getProfileOptions();
		if (newProfile == null)
		{
			new MessageBox(window, profilesBundle.getString("errorWindowTitle")
					, profilesBundle.getString("errorProfileName"));
			return;
		}
		if (selected == -1)
		{
			// create new profile
			profilesList.add(newProfile);
			// write to ini
			addProfileToIni(newProfile);

			try
			{
				profilesIni.store(profilesFile);
			} catch (IOException e)
			{
				new MessageBox(window, profilesBundle.getString("errorWindowTitle")
						, profilesBundle.getString("errorSaveProfile") +
						e.getMessage());
				// rollback
				profilesList.remove(newProfile);
				profilesIni.remove(newProfile.getName());
			}
		} else
		{
			// update selected profile
			Profile oldProfile = profilesList.get(selected);
			profilesList.set(selected, newProfile);
			// write to ini file
			profilesIni.remove(oldProfile.getName());
			addProfileToIni(newProfile);
			try
			{
				profilesIni.store(profilesFile);
			} catch (IOException e)
			{
				new MessageBox(window, profilesBundle.getString("errorWindowTitle")
						, profilesBundle.getString("errorSaveProfile") +
						e.getMessage());
				// rollback
				profilesIni.remove(newProfile.getName());
				addProfileToIni(oldProfile);
			}
		}
	}

	// --------------------------------------------
	private void addProfileToIni(Profile profile)
	{
		Section newSection = profilesIni.add(profile.getName());
		newSection.add("Width", profile.getWidth());
		newSection.add("Height", profile.getHeight());
		newSection.add("LSoft", profile.getlSoft());
		newSection.add("RSoft", profile.getrSoft());
		newSection.add("Menu", profile.getMenu());
		newSection.add("OnUp", profile.getOnUp());
		newSection.add("OnDown", profile.getOnDown());
		newSection.add("OnLeft", profile.getOnLeft());
		newSection.add("OnRight", profile.getOnRight());
	}

	// --------------------------------------------
	private Profile getProfileOptions()
	{
		Profile profile = new Profile();
		String name = nameTF.getText();
		if (name == null || name.length() == 0)
		{
			return null;
		}
		profile.setName(name);

		int width = 176;
		String widthString = widthTF.getText();
		try
		{
			width = Integer.valueOf(widthString);
		} catch (Exception e)
		{
		}
		profile.setWidth(width);

		int height = 220;
		String heightString = heightTF.getText();
		try
		{
			height = Integer.valueOf(heightString);
		} catch (Exception e)
		{
		}
		profile.setHeight(height);

		int lSoft = -6;
		String lSoftString = lSoftTF.getText();
		try
		{
			lSoft = Integer.valueOf(lSoftString);
		} catch (Exception e)
		{
		}
		profile.setlSoft(lSoft);

		int rSoft = -7;
		String rSoftString = rSoftTF.getText();
		try
		{
			rSoft = Integer.valueOf(rSoftString);
		} catch (Exception e)
		{
		}
		profile.setrSoft(rSoft);

		int menu = 0;
		String menuString = menuTF.getText();
		try
		{
			menu = Integer.valueOf(menuString);
		} catch (Exception e)
		{
		}
		profile.setMenu(menu);

		Navigation selectedNavigation = null;
		Navigation onUp = Navigation.UP_SCREEN;
		selectedNavigation = onUpCB.getSelectionModel().getSelectedItem();
		if (selectedNavigation != null)
			onUp = selectedNavigation;
		Navigation onDown = Navigation.DOWN_SCREEN;
		selectedNavigation = onDownCB.getSelectionModel().getSelectedItem();
		if (selectedNavigation != null)
			onDown = selectedNavigation;
		Navigation onLeft = Navigation.UP_LINE;
		selectedNavigation = onLeftCB.getSelectionModel().getSelectedItem();
		if (selectedNavigation != null)
			onLeft = selectedNavigation;
		Navigation onRight = Navigation.DOWN_LINE;
		selectedNavigation = onRightCB.getSelectionModel().getSelectedItem();
		if (selectedNavigation != null)
			onRight = selectedNavigation;
		profile.setOnUp(onUp.getKey());
		profile.setOnDown(onDown.getKey());
		profile.setOnLeft(onLeft.getKey());
		profile.setOnRight(onRight.getKey());

		return profile;
	}

	// --------------------------------------------
	@Override
	public void initialize(URL location, ResourceBundle bundle)
	{
		profilesBundle = bundle;
		
		profileTV.setItems(profilesList);
		profilesList.addListener(new ListChangeListener<Profile>()
		{
			@Override
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends Profile> change)
			{
				while (change.next())
				{
					if (change.wasPermutated())
					{
						profileTV.getSelectionModel().clearSelection();
					}
				}
			}

		});

		ContextMenu profileTVCM = new ContextMenu();
		MenuItem profileTVDeleteMI = new MenuItem(profilesBundle.getString("deleteMenu"));
		profileTVDeleteMI.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent ae)
			{
				Profile selected = profileTV.getSelectionModel()
						.getSelectedItem();
				if (selected == null)
					return;
				
				// remove from ini file
				profilesIni.remove(selected.getName());
				try
				{
					profilesIni.store(profilesFile);
					profilesList.remove(selected);
				} catch (IOException e)
				{
					new MessageBox(window
							, profilesBundle.getString("errorWindowTitle")
							, profilesBundle.getString("deleteProfileError") +
							e.getMessage());
					return;
				}
			}
		});
		MenuItem profileTVCreateMI = new MenuItem(profilesBundle.getString("createMenu"));
		profileTVCreateMI.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent ae)
			{
				profileTV.getSelectionModel().clearSelection();
				clearForm();
			}
		});
		profileTVCM.getItems().add(profileTVCreateMI);
		profileTVCM.getItems().add(profileTVDeleteMI);
		profileTV.setContextMenu(profileTVCM);

		navigationActionsList = FXCollections.observableArrayList();
		navigationActionsList.addAll(Navigation.values());
		onUpCB.setItems(navigationActionsList);
		onDownCB.setItems(navigationActionsList);
		onLeftCB.setItems(navigationActionsList);
		onRightCB.setItems(navigationActionsList);

		// read profiles from file
		try
		{
			if(!profilesFile.exists())
			{
				new MessageBox(window, profilesBundle.getString("errorWindowTitle")
						, profilesBundle.getString("errorFileNotExists"));
				profilesFile.createNewFile();
			}
			profilesIni = new Ini(profilesFile);
			Set<String> profilesSet = profilesIni.keySet();
			for (String profileName : profilesSet)
			{
				Profile profile = new Profile();
				profile.setName(profileName);
				try
				{
					Section profileSection = profilesIni.get(profileName);
					int width = profileSection.get("Width", Integer.class);
					profile.setWidth(width);
					int height = profileSection.get("Height", Integer.class);
					profile.setHeight(height);
					int lSoft = profileSection.get("LSoft", Integer.class);
					profile.setlSoft(lSoft);
					int rSoft = profileSection.get("RSoft", Integer.class);
					profile.setrSoft(rSoft);
					int menu = profileSection.get("Menu", Integer.class);
					profile.setMenu(menu);
					int onUp = profileSection.get("OnUp", Integer.class);
					profile.setOnUp(onUp);
					int onDown = profileSection.get("OnDown", Integer.class);
					profile.setOnDown(onDown);
					int onLeft = profileSection.get("OnLeft", Integer.class);
					profile.setOnLeft(onLeft);
					int onRight = profileSection.get("OnRight", Integer.class);
					profile.setOnRight(onRight);
				} catch (Exception e)
				{
					
					new MessageBox(window, profilesBundle.getString("errorWindowTitle")
							, profilesBundle.getString("errorInProfile").replace("%profile", profileName)
							);
					continue;
				}
				profilesList.add(profile);
			}
		}  catch (InvalidFileFormatException e)
		{
			new MessageBox(window, profilesBundle.getString("errorWindowTitle")
					, profilesBundle.getString("errorFileFormat"));
		} catch (IOException e)
		{
			new MessageBox(window, profilesBundle.getString("errorWindowTitle")
					, profilesBundle.getString("errorFileRead"));
		} catch (Exception e)
		{
			new MessageBox(window, profilesBundle.getString("errorWindowTitle")
					, profilesBundle.getString("errorFileRead"));
		}

		// show options of selected profile
		profileTV.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<Profile>()
				{

					@Override
					public void changed(
							ObservableValue<? extends Profile> arg0,
							Profile oldProfile, Profile newProfile)
					{
						if (newProfile == null)
							return;
						String name = newProfile.getName();
						nameTF.setText(name);

						int width = newProfile.getWidth();
						widthTF.setText(String.valueOf(width));
						int height = newProfile.getHeight();
						heightTF.setText(String.valueOf(height));

						int lSoft = newProfile.getlSoft();
						lSoftTF.setText(String.valueOf(lSoft));
						int rSoft = newProfile.getrSoft();
						rSoftTF.setText(String.valueOf(rSoft));
						int menu = newProfile.getMenu();
						menuTF.setText(String.valueOf(menu));

						int onUp = newProfile.getOnUp();
						onUpCB.getSelectionModel().select(
								Navigation.valueOf(onUp));
						int onDown = newProfile.getOnDown();
						onDownCB.getSelectionModel().select(
								Navigation.valueOf(onDown));
						int onLeft = newProfile.getOnLeft();
						onLeftCB.getSelectionModel().select(
								Navigation.valueOf(onLeft));
						int onRight = newProfile.getOnRight();
						onRightCB.getSelectionModel().select(
								Navigation.valueOf(onRight));
					}
				});

	}

	// --------------------------------------------
	private void clearForm()
	{
		nameTF.clear();
		widthTF.clear();
		heightTF.clear();
		lSoftTF.clear();
		rSoftTF.clear();
		menuTF.clear();
		onUpCB.getSelectionModel().clearSelection();
		onDownCB.getSelectionModel().clearSelection();
		onLeftCB.getSelectionModel().clearSelection();
		onRightCB.getSelectionModel().clearSelection();
	}
}
