package com.qwertovsky.mobibook.generator;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Previewer
{
	private String text;
	private List<String> linesList = new ArrayList<String>();
	private int line = 0;
	private int lineInScreen;
	private double height;
	private Font font;

	// ------------------------------------------
	public void setText(byte[] textBytes) throws UnsupportedEncodingException
	{
		this.text = new String(textBytes, "windows-1251");
		String[] lines = text.split("\n");
		linesList.clear();
		for (String line : lines)
		{
			linesList.add(line + "\n");
		}
	}

	// ------------------------------------------
	public void setHeight(double height)
	{
		this.height = height;
		updateLinesInScreen();
	}

	public void setFont(Font font)
	{
		this.font = font;
		updateLinesInScreen();
	}

	// ------------------------------------------
	private void updateLinesInScreen()
	{
		String screen = "";
		// update count lines in screen
		Text text = new Text();
		text.setFont(font);

		if (linesList.isEmpty())
			return;

		lineInScreen = 0;
		for (String s : linesList)
		{
			text.setText((screen + s).trim());

			double textHeight = text.getLayoutBounds().getHeight();
			if (textHeight > height)
			{
				break;
			}
			screen = screen + s;
			lineInScreen++;
		}
	}

	// ------------------------------------------
	public String getScreen(int line)
	{
		String screen = "";
		int startLine = line;
		if (startLine >= linesList.size())
			return null;
		int endLine = line + lineInScreen;
		if (endLine > linesList.size())
			endLine = linesList.size();
		for (String s : linesList.subList(startLine, endLine))
		{
			screen = screen + s;
		}

		return screen;
	}

	// ------------------------------------------
	public String getPrevScreen()
	{
		if (line > 0)
		{
			line = line - lineInScreen;
			if (line < 0)
				line = 0;
		}
		return getScreen(line);
	}

	// ------------------------------------------
	public String getNextScreen()
	{
		if (line + lineInScreen < linesList.size())
			line = line + lineInScreen;
		return getScreen(line);
	}

	// --------------------------------------------
	public String getNextLine()
	{
		if (line + lineInScreen < linesList.size())
			line++;
		return getScreen(line);
	}

	// ------------------------------------------
	public String getPrevLine()
	{
		if (line > 0)
			line--;
		return getScreen(line);
	}
}
