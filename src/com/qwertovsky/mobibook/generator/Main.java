package com.qwertovsky.mobibook.generator;

import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application
{
	Scene scene = null;
	MainWindowController controller = null;
	ResourceBundle mainBundle;
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		launch(args);

	}
	
	@Override
	public void start(Stage stage) throws Exception
	{
		
		try
		{
			URL fxml = new File("resources/MainWindow.xml").toURI().toURL();
			FXMLLoader loader = new FXMLLoader(fxml);
			try
			{
				mainBundle = ResourceBundle.getBundle("MainWindow");
			} catch(Exception e)
			{
				Locale locale = new Locale("ru","RU");
				mainBundle = ResourceBundle.getBundle("MainWindow", locale);
			}
			loader.setResources(mainBundle);
			scene = (Scene) loader.load();
			controller = loader.getController();
			controller.window = stage;
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		stage.setScene(scene);
		stage.setMinWidth(scene.getWidth());
		stage.setTitle("MobiBook QwertoGenerator");
		stage.getIcons().add(new Image("file:resources/book.png"));
		
		stage.setOnCloseRequest(new EventHandler<WindowEvent>()
				{

					@Override
					public void handle(WindowEvent paramT)
					{
						controller.saveConfig();
					}
				});
		
		stage.show();

	}

	

}
