package com.qwertovsky.mobibook.generator;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class MessageBox
{
	public MessageBox(Window window, String title, String message)
	{
		final Stage stage = new Stage();
		stage.initOwner(window);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UTILITY);
		stage.setTitle(title);
		
		BorderPane root = new BorderPane();
		root.setPadding(new Insets(10));
		
		Scene scene = new Scene(root, 300, 150);
		stage.setScene(scene);
		
		TextArea messageTA = new TextArea(message);
		messageTA.setEditable(false);
		messageTA.setMinSize(0, 0);
		root.setCenter(messageTA);
		BorderPane.setMargin(messageTA, new Insets(0,0,5,0));
		
		HBox buttonBox = new HBox();
		buttonBox.setAlignment(Pos.CENTER_RIGHT);
		buttonBox.setSpacing(15);
		root.setBottom(buttonBox);
		
		Button okB = new Button("OK");
		okB.setOnAction(new EventHandler<ActionEvent>()
				{
					@Override
					public	void handle(ActionEvent event)
					{
						stage.close();
					}
				});
		buttonBox.getChildren().add(okB);
		
		if(window != null)
		{
			stage.setX(window.getX()+(window.getWidth()-scene.getWidth()) / 2);
			stage.setY(window.getY() + (window.getHeight()-scene.getHeight()) / 2);
		}
		stage.showAndWait();
	}
}
