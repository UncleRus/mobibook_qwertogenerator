package com.qwertovsky.mobibook.generator;

import java.util.Vector;

public class Hyphenator
{
	private static String x = "йьъ";
	private static String g = "аеёиоуыэюяaeiouy";
	private static String s = "бвгджзклмнпрстфхцчшщbcdfghjklmnpqrstvwxz";
	private static Vector<HyphenPair> rules = new Vector<HyphenPair>();

	public static void initialize()
	{
		rules.addElement(new HyphenPair(" ", 1));
		rules.addElement(new HyphenPair("xgg", 1));
		rules.addElement(new HyphenPair("xgs", 1));
		rules.addElement(new HyphenPair("xsg", 1));
		rules.addElement(new HyphenPair("xss", 1));
		rules.addElement(new HyphenPair("gssssg", 3));
		rules.addElement(new HyphenPair("gsssg", 3));
		rules.addElement(new HyphenPair("gsssg", 2));
		rules.addElement(new HyphenPair("sgsg", 2));
		rules.addElement(new HyphenPair("gssg", 2));
		rules.addElement(new HyphenPair("sggg", 2));
		rules.addElement(new HyphenPair("sggs", 2));
	}
	
	//--------------------------------------------
	/**
	 * Разбивает текст на слоги
	 * @param text 
	 * @return массив слогов
	 */
	public static Vector<String> hyphenateWord(String text)
	{
		if(rules.isEmpty())
			initialize();
		
		//determine ' ', 'x', 's' and 'g'
		StringBuffer sb = new StringBuffer();
		String lowerText = text.toLowerCase();
		for (int i = 0; i < lowerText.length(); i++)
		{
			char c = lowerText.charAt(i);
			if (c == ' ')
			{
				sb.append(" ");
			}
			else if (x.indexOf(c) != -1)
			{
				sb.append("x");
			}
			else if (g.indexOf(c) != -1)
			{
				sb.append("g");
			}
			else if (s.indexOf(c) != -1)
			{
				sb.append("s");
			}
			else
			{
				sb.append(c);
			}
		}
		String hyphenatedText = sb.toString();
		
		//find hyphenations
		for (int i = 0; i < rules.size(); i++)
		{
			HyphenPair hp = rules.elementAt(i);
			int index = hyphenatedText.indexOf(hp.pattern);
			while (index != -1)
			{
				int actualIndex = index + hp.position;
				hyphenatedText =
						hyphenatedText.substring(0, actualIndex) + (char)1
								+ hyphenatedText.substring(actualIndex);
				text =
						text.substring(0, actualIndex) + (char)1
								+ text.substring(actualIndex);
				index = hyphenatedText.indexOf(hp.pattern, actualIndex);
			}
		}
		
		//get array of syllables
		String[] parts = text.split(String.valueOf((char)1));
		Vector<String> result = new Vector<String>(parts.length);
		for (int i = 0; i < parts.length; i++)
		{
			String value = parts[i];
			result.addElement(value);
		}
		return result;
	}
	
	//--------------------------------------------
	/**
	 * Делает перенос в тексте по слогам или по пробелу между словами.
	 * Оставляет на строке необходимое количество символов.
	 * @param text
	 * @param maxLenght Максимальная длина текста, который может поместиться на строчке.
	 * @return Две строки. Вторая строка должна быть перенесена на новую строчку. 
	 */
	public static Vector<String> hyphenateWord(String text, int maxLenght)
	{
		Vector<String> parts = hyphenateWord(text);
		Vector<String> result = new Vector<String>(2);
		
		int i = 0;
		String firstPart = "";
		while(i < parts.size() && (firstPart+parts.elementAt(i)).length() < maxLenght)
		{
			firstPart += parts.elementAt(i);
			i++;
		}
		if(firstPart.endsWith(" ") || firstPart.endsWith("\n"))
			firstPart = firstPart.trim();
		else if(i < parts.size())
			firstPart += "-";
		result.add(firstPart);
		
		String secondPart = "";
		for(int j = i; j < parts.size(); j++)
		{
			secondPart += parts.elementAt(j);
		}
		result.add(secondPart);
		return result;
	}
	
	//--------------------------------------------
	/**
	 * @author dfkthbq
	 *
	 */
	private static class HyphenPair
	{
		public String pattern;
		public int position;

		public HyphenPair(String pattern, int position)
		{
			this.pattern = pattern;
			this.position = position;
		}
	}
}
